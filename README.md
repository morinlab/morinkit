# Morinkit: Python functions and scripts for genomics

[![Build Status](https://travis-ci.org/morinlab/morinkit.svg?branch=master)](https://travis-ci.org/morinlab/morinkit)
[![Coverage Status](https://coveralls.io/repos/github/morinlab/morinkit/badge.svg?branch=master)](https://coveralls.io/github/morinlab/morinkit?branch=master)

## Installation

You need Python 3.3 or later to installing morinkit. 

```bash
pip install morinkit
```

## Dependencies

These are automatically installed when you run `pip install morinkit`. 

* Python 3.3 or later
* [These Python modules](requirements.txt)
